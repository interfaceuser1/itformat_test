<?php
require $_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php";
use \Bitrix\Main\Loader;

/**
 * алгоритм:
 * торговые предложения могут быть переданы из 1с или например сформированы отдельным
 * скриптом(в том числе автоматическим по заданным параметрам).
 * тут описан класс принимающий либо имя либо id товара и массив набора торговых предложений описываемых
 * свойствами и собственно добавляющий их в базу.
 * методы добавления предложений возвращают true если все успешно или же массив строк с ошибками в противном случае
 * предполагается что идентификаторы соотвествующих инфоблоков передаваемых в класс актуальны иначе можно тут наворотить
 * разных проверок на 10 мегабайт кода
 * в функции добавления торг предл передается массив с элементами типа FIELDS и PROPS каждый из которых в свою очередь
 * массив. для FIELDS поля IBLOCK и ACTIVE заполняются автоматически и перезапишут переданные в параметре. Также ожидается
 * элемент PRICE содержащий цену. Если он будет отсуствовать цена будет установлена в 1 единицу. Ожидаются параметры CURRENCY
 * QUANTITY_FROM QUANTITY_TO CATALOG_GROUP_ID которые в случае отсуствия будут установлены по умолчанию
 */

define('CATALOG_IBLOCK_ID', 1);
define('TORG_PREDL_IBLOCK_ID', 2);
define('DEFAULT_PRICE', 1);
define('DEFAULT_QTY', 1);
define('DEFAULT_CURRENCY', 'RUR');
define('DEFAULT_QUANTITY_FROM', 1);
define('DEFAULT_QUANTITY_TO', 1);
define('DEFAULT_CATALOG_GROUP_ID', 1);

class torgPredl
{
    private $catalog_ib_id = null;
    private $torg_predl_ib_id = null;

    public function __construct($catalog_ib_id, $torg_predl_ib_id)
    {
        if (!Loader::includeModule('iblock') || !Loader::includeModule('catalog')) {
            die('Error loading module iblock or catalog');
        }
        $this->catalog_ib_id = $catalog_ib_id;
        $this->torg_predl_ib_id = $torg_predl_ib_id;
    }

    /**
     * предварительно создает элемент каталога с указанным именем и к нему уже добавляет торг предл
     */
    public function add_torg_predl_for_name($name, $torg_predl_list)
    {
        $id = $this->get_id_by_name($name);
        if ($id === false) {
            $fields = [
                'NAME' => $name,
                'ACTIVE' => 'Y',
                'IBLOCK_ID' => $this->catalog_ib_id,
            ];
            $el = new CIblockElement();
            $id = $el->add($fields);
        }
        if(!$id){
            return ['ошибка добавления товара с именем ' . $name . '; error = ' . $el->LAST_ERROR];
        }
        return $this->add_tord_predl_for_id($id, $torg_predl_list);
    }

    /**
     * добавляет торг предл к уже имеющемуся товару с указанным id
     */
    public function add_tord_predl_for_id($id, $torg_predl_list)
    {
        $arCatalog = CCatalog::GetByID($this->torg_predl_ib_id);
        $IBlockCatalogId = $arCatalog['PRODUCT_IBLOCK_ID'];
        $SKUPropertyId = $arCatalog['SKU_PROPERTY_ID'];
        
        $el = new CIBlockElement();

        foreach($torg_predl_list as $torg_predl_item){
            $offer_props = $torg_predl_item['PROPS'];
            $offer_props[$SKUPropertyId] = $id;

            $offer_fields = $torg_predl_item['FIELDS'];
            $offer_fields['IBLOCK_ID'] = $this->torg_predl_ib_id;
            $offer_fields['ACTIVE'] = 'Y';
            $offer_fields['PROPERTY_VALUES'] = $offer_props;

            $offer_id = $el->add($offer_fields);
            if($offer_id){
                $catalog_offer_id = CCatalogProduct::add([
                    'ID' => $offer_id,
                    'VAT_INCLUDED' => 'Y',
                    'QUANTITY' => DEFAULT_QTY,
                ]);

                $price_fields = [
                    'PRODUCT_ID' => $offer_id,
                ];
                if($torg_predl_item['CURRENCY']){
                    $price_fields['CURRENCY'] = $torg_predl_item['CURRENCY'];
                }else{
                    $price_fields['CURRENCY'] = DEFAULT_CURRENCY;
                }

                if($torg_predl_item['QUANTITY_FROM']){
                    $price_fields['QUANTITY_FROM'] = $torg_predl_item['QUANTITY_FROM'];
                }else{
                    $price_fields['QUANTITY_FROM'] = DEFAULT_QUANTITY_FROM;
                }

                if($torg_predl_item['QUANTITY_TO']){
                    $price_fields['QUANTITY_TO'] = $torg_predl_item['QUANTITY_TO'];
                }else{
                    $price_fields['QUANTITY_TO'] = DEFAULT_QUANTITY_TO;
                }

                if($torg_predl_item['CATALOG_GROUP_ID']){
                    $price_fields['CATALOG_GROUP_ID'] = $torg_predl_item['CATALOG_GROUP_ID'];
                }else{
                    $price_fields['CATALOG_GROUP_ID'] = DEFAULT_CATALOG_GROUP_ID;
                }

                if($torg_predl_item['PRICE']){
                    $price_fields['PRICE'] = $torg_predl_item['PRICE'];
                }else{
                    $price_fields['PRICE'] = DEFAULT_PRICE;
                }

                $price_result = CPrice::Add($price_fields);

                if($price_result === false){

                    return ['ошибка добавления цены для торгового предложения. Состав параметров цены:' . print_r($price_fields, true)];

                }


            }else{
                return ['ошибка добавления торгового предложения с именем ' . $name . ' для товара id=' . $id . '; error = ' . $el->LAST_ERROR];
            }
        }
        

    }

    private function get_id_by_name($name)
    {
        $filter = ['IBLOCK_ID' => $this->catalog_ib_id, 'NAME' => $name];
        $select = ['ID'];
        $res = \Bitrix\Iblock\ElementTable::getList([
            'filter' => $filter,
            'select' => $select,
        ]);
        if ($res->getSelectedRowsCount() > 0) {
            return $res->fetch()['ID'];
        } else {
            return false;
        }
    }
}
