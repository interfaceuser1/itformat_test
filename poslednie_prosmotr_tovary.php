<?php
require $_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php";


/**
 * алгоритм:
 * - сначала нужно собрать информацию о просмотрах товаров. это может делать например яваскрипт код в шаблоне карточки товара
 * отправляющий аякс запрос с id товара на бэк где этот id будет записан в очередь которая лежит в сессии. Либо же подобную запись
 * в очередь может делать result_modifier.php. На выходе имеем очередь в виде массива с id просмотренных товаров
 * - тут описана функция add_to_chain которая собственно пишет в очередь и может вызываться в вышеописанных скриптах
 * - тут описана функция return_chain которая возвращает массив последних просмотренных товаров
 * - тут описан обработчик события создания заказа который вытащит из переданного в параметре объекта заказа id заказа
 * и сформирует письмо(предполагается что шаблон письма уже создан и подготовлен под передаваемые в него параметры)
 * Параметры для шаблона:
 * LAST_SEEN_CATALOG_ITEMS - строка с именами товаров(тут в общем то на основе id товаров можно сформировать что угодно но пусть будет
 * список имен) разделенных переносом строки <br>
 * ORDER_ID - id заказа. просмотр заказа позволит выяснить контакты юзера
 * 
 * тут возможно стоит обсудить вопрос - стоит ли записывать только уникальные просмотры. С одной стороны это позволит увидеть больше
 * разных товаров которые смотрел юзер. С другой стороны если писать все подряд то можно увидеть товары котоыре юзер смотрел
 * несколько раз и вероятно они его зацепили. В данном случае я пишу все подряд.
 */

 define('CHAIN_LENGTH', 10);
 define('LAST_SEEN_SESSION_KEY', 'last_seen_items');
 define('CATALOG_IBLOCK_ID', 1);

 function add_to_chain($item_id){
    $chain = $_SESSION[LAST_SEEN_SESSION_KEY];
    if(count($chain) == CHAIN_LENGTH){
        //тут делаем продвижение очереди т.е. самый старый элемент удаляется а в конец добавляется самый новый
        $temp = array_slice($chain, 1);
        $temp[] = $item_id;
        $chain = $temp;

    }else{
        $chain[] = $item_id;
    }

    $_SESSION[LAST_SEEN_SESSION_KEY] = $chain;
 }

 function return_chain(){
    if(is_array($_SESSION[LAST_SEEN_SESSION_KEY])){
        return $_SESSION[LAST_SEEN_SESSION_KEY];
    }else{
        return [];
    }
 }

/**
 * подключаем обработчик. обычно в events.php который инклудится в init.php
 */

use Bitrix\Main; 
use \Bitrix\Main\Loader;
Main\EventManager::getInstance()->addEventHandler(
    'sale',
    'OnSaleComponentOrderCreated',
    'order_saved_handler'
);

function order_saved_handler($order){

    Loader::includeModule('iblock');

    $items = return_chain();
    $items_string = '';
    if(count($items) > 0){
        $filter = ['IBLOCK_ID' => CATALOG_IBLOCK_ID, 'ID' => $items];
        $select = ['NAME'];
        $res = \Bitrix\Iblock\ElementTable::getList([
            'filter' => $filter,
            'select' => $select
        ]);
        while($item = $res->fetch()){
            $items_string .= $item['NAME'] . '<br>';
        }   


        $order_id = $order->getId();

        $mail_fields = [
            'LAST_SEEN_CATALOG_ITEMS' => $items_string,
            'ORDER_ID' => $order_id,
        ];

        CEvent::Send('LAST_SEEN_ITEMS', SITE_ID, $mail_fields);
    }
    
}
