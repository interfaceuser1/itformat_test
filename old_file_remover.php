<?php
require $_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php";

/**
 * алгоритм:
 * - собираем содержимое каталога с файлами где предполагается наличие неучтенных в базе файлов рекурсивно
 * - делаем выборку в память всей таблицы b_file с полями SUBDIR FILE_NAME 
 * - вычитаем 2ю выборку из первой и получаем массив путей к файлам которые подлежат удалению как неучтенные в базе
 */

 $path = $_SERVER['DOCUMENT_ROOT'] . '/upload';
 
/**
 * рекурсивно обходит каталог собирая пути файлов
 */
function find_all_files($dir)
{
    $root = scandir($dir);
    foreach ($root as $value) {
        if ($value === '.' || $value === '..') {
            continue;
        }
        if (is_file("$dir/$value")) {
            $result[]="$dir/$value";
            continue;
        }
        foreach (find_all_files("$dir/$value") as $value) {
            $result[]=$value;
        }
    }
    return $result;
}

$all_files = find_all_files($path);
//удаляем из путей часть до требуемого каталога
for($i = 0; $i < count($all_files); $i++){
    $all_files[$i] = substr($all_files[$i], strlen($path) + 1);
}

global $DB;

$q = "select SUBDIR, FILE_NAME from b_file;";
$res = $DB->Query($q);

while($db_file = $res->getNext()){
    $db_file_path = $db_file['SUBDIR'] . '/' . $db_file['FILE_NAME'];
    $id = array_search($db_file_path, $all_files);
    if($id !== false){
        unset($all_files[$id]);
    }
}

/**
 * сейчас в $all_files лежат пути к файлам которые есть в указанном каталоге но не учтены в базе. Можно их например отфильтровать
 * по расширению или по системным именам типа .htaccess и делать с ними что нужно.
 */

